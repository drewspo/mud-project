package mud

class PriorityQueue[A](higherP: (A, A) => Boolean) {
  class Node(val data: A, var next: Node)
  var head: Node = null  
  def enqueue(o: A): Unit = {
    if (head == null || higherP(o, peek)) {
      head = new Node(o, head)
    } else {
      var rover = head
      while (rover.next != null && higherP(rover.next.data, o)) {
        rover = rover.next
      }
      rover.next = new Node(o, rover.next)
    }
  }
  def dequeue(): A = {
    val tmp = peek
    head = head.next
    tmp
  }
  def changeAll(alter: A => A):Unit = {
    var rover = head
    while(rover != null) {
      alter(rover.data)
      rover = rover.next
    }
  }
  def next:Node = head
  def peek: A = head.data
  def isEmpty: Boolean = head == null
}