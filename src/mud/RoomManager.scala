

package mud

import scala.io.Source
import scala.io.StdIn._
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import scala.collection.mutable.ArrayBuffer

class RoomManager extends Actor {
  var players: Map[String, Player] = null
  val source = Source.fromFile("map.txt")
  val lines = source.getLines
  val numOfRooms = lines.next.toInt
  var roomMap = new BSTMap[Int, RoomManager.RoomClass]((a: Int, b: Int) => a - b)
  var rooms = new BSTMap[Int, ActorRef]((a: Int, b: Int) => a - b)
  var roomNames = List[RoomManager.RoomName]()
  private var playerManager: ActorRef = null
  private var activityManager:ActorRef = null
  def setup(): Unit = {
    for (a <- 0 until numOfRooms) {
      val nameLine = lines.next
      val num = nameLine.split(" ")(0).toInt
      val name = nameLine.split(" ")(1)
      val desc = lines.next
      var numOfDest = lines.next.toInt
      var dests = {
        (for (j <- 1 to numOfDest) yield {
          var parts = lines.next.split(" ")
          new RoomManager.Destination(parts(0), parts(1)toInt, parts(2))
        }).toArray
      }
      val numOfItems = lines.next.toInt
      var items = List[Item]()
      for (j <- 1 to numOfItems) {
        val nextItem = lines.next.split(" ")
        if (nextItem.length == 2) {
          items ::= new Item(nextItem(0), nextItem(1).toInt)
        } else {
          items ::= new Item(nextItem(0)+" "+nextItem(1), nextItem(2).toInt)
        }
      }
      roomNames ::= new RoomManager.RoomName(num, name)
      roomMap = roomMap += (num -> RoomManager.RoomClass(num, name.toString, name.toString, desc, items, dests))
      rooms = rooms += (num -> context.actorOf(Props(new Room(num, name.toString, name.toString, desc, items, dests))))
    }
  }
  context.children.foreach(room => room ! Room.LinkExits(rooms, roomNames))

  import RoomManager._

  def receive = {
    case NCPRoom => {
      sender ! NCP.RoomMapReceived(rooms)
    }
    case InitialEnterRoom(player, roomKey, n, pManager, aManager) => {
      if (n < 2) {
        playerManager = pManager
        activityManager = aManager
        setup
      }
      for (i <- 0 until numOfRooms) {
        rooms(i) ! Room.LinkExits(rooms, roomNames)
      }
      player ! Player.FirstMove(rooms(roomKey), self, playerManager, aManager)
    }
    case ShortestPath(cur, place) => {
      var index = 0
      var flag = true
      var dest = -1
      while (index < roomNames.length && flag) {
        if (roomNames(index).name == place) {
          dest = roomNames(index).num
          flag = false
        } else {
          index += 1
        }
      }
      if (dest < 0) sender ! Player.InvalidRoom else
        sender ! Player.GotPath(shortestPath(cur, dest, roomMap, Set()))
    }
    case GetDestination(r, i) => {
    }
  }

  def shortestPath(cur: Int, dest: Int, map: BSTMap[Int, RoomClass], visited: Set[Int]): List[String] = {
    if (cur == dest) List(map(dest).name)
    else if (visited(cur)) Nil
    else {
      val ret = for (i <- 0 until map(cur).exitKeys.length) yield {
        val sp: List[String] = shortestPath(map(cur).exitKeys(i).num, dest, map, visited + cur)
        if (sp.isEmpty) Nil else map(cur).name :: sp
      }
      val nonEmpty = ret.filter(_.nonEmpty)
      if (nonEmpty.isEmpty) Nil else nonEmpty.minBy { x => x.length }
    }
  }
}

object RoomManager {
  case object NCPRoom
  case class RoomName(num: Int, name: String)
  case class ShortestPath(cur: Int, dest: String)
  case class InitialEnterRoom(player: ActorRef, roomKey: Int, n: Int, pManager: ActorRef, aManager:ActorRef)
  case object Setup
  case class GetDestination(roomName: String, i: Int)
  case class RoomClass(val num: Int, val keyword: String, val name: String, val desc: String, private var items: List[Item], exitKeys: Array[RoomManager.Destination])
  case class Destination(dir: String, num: Int, name: String)
}

