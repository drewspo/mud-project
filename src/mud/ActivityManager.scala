
package mud

import scala.collection.mutable
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import scala.util.Random
import ActivityManager._

class ActivityManager extends Actor {
  private val activityQueue = new BinaryHeap[Activity](_.execute < _.execute)
  private val NCPs = mutable.ArrayBuffer[NCPStorable]()
  private var roomManager: ActorRef = null
  private var players = Map[String, ActorRef]()
  private var counter:Long = 0

  val damageRange = 30
  val r = scala.util.Random
  def receive = {
    case CheckQueue => {
      checkQueue
      counter += 1
    }
    case PlayerManager.NewPlayer(playerName: String, newPlayer: ActorRef) => {
      players = players + (playerName -> newPlayer)
    }
    case AttackNCP(target, weapon, playerName) => {
      var ncpList = NCPs.filter(_.name == target)
      if (ncpList.size > 0) {
        sender ! Player.EnterCombat
        val damage = r.nextInt(damageRange)
        activityQueue.enqueue(new Activity(counter+weapon.speed, ncpList(0).ncp, target, sender, playerName, damage))
      } else {
        println("looking for "+target)
        sender ! Player.InvalidCommand("That bug isn't here.")
      }
    }
    case InitializeNCPs(rManager) => {
      roomManager = rManager
      initializeNCPs
    }
    case BugDied(bugName, actor) => {
      NCPs -= NCPStorable(bugName, actor)
    }
    case AttackPlayer(playerName, time, bugName, damage) => {
      activityQueue.enqueue(new Activity(counter+time, players(playerName), playerName, sender, bugName, damage))
    }
    case Player.PlayerDead(playerName, playerRef) => {
      playerDead(playerName, playerRef)
    }
  }
  def initializeNCPs = {
    NCPs += NCPStorable("Tarantula", context.actorOf(Props(new NCP("Tarantula", 100))))
    NCPs += NCPStorable("Bee", context.actorOf(Props(new NCP("Bee", 100))))
    NCPs += NCPStorable("Ant", context.actorOf(Props(new NCP("Ant", 100))))
    NCPs += NCPStorable("Hornet", context.actorOf(Props(new NCP("Hornet", 100))))
    NCPs += NCPStorable("Fly", context.actorOf(Props(new NCP("Fly", 100))))
    for (i <- NCPs) i.ncp ! NCP.InitializeNCP(roomManager, self)
  }
  def checkQueue: Unit = {
    if (activityQueue != null && !activityQueue.isEmpty) {
      val current = counter
      while (!activityQueue.isEmpty && activityQueue.peek.execute < current) {
        val tmp = activityQueue.dequeue()
        tmp.target ! tmp
      }
    }
  }
  def playerDead(playerName:String, playerRef:ActorRef) = {
    players = players - playerName
  }
}
object ActivityManager {
  case class ExitNCPCombat(bugName:String)
  case class AttackPlayer(playerName:String, time:Long, bugName:String, damage:Int)
  case class BugDied(bugName:String, actor:ActorRef)
  case object CheckQueue
  case class NCPStorable(name: String, ncp: ActorRef)
  case class Activity(var execute: Long, target: ActorRef, targetName:String, killer:ActorRef, killerName:String, damage: Int)
  case class AttackNCP(target: String, weapon: Item, playerName: String)
  case class InitializeNCPs(rManager: ActorRef)
}