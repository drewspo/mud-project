package mud

import scala.io.StdIn.readLine
import akka.actor.ActorSystem
import akka.actor.Actor
import akka.pattern._
import akka.util.Timeout
import akka.actor.Props
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.ActorRef
import scala.io.Source
import java.net.Socket
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.InputStreamReader
import java.io.BufferedOutputStream
import java.io.PrintStream
import java.io.BufferedReader

class Player(name: String, private var inventory: List[Item], val sock: Socket, val is: PrintStream, val os: BufferedReader, private var health: Int) extends Actor {
  val instruc = Source.fromFile("instructions.txt")
  var instrucLines_ = instruc.reset.getLines.toArray
  def instrucLines = instrucLines_
  def inv = inventory.length
  private var equippedItem: Item = null
  private var input = ""
  private var currentRoom: ActorRef = null
  private var roomManager: ActorRef = null
  private var playerManager: ActorRef = null
  private var activityManager: ActorRef = null
  private var currentRoomName: String = null
  private var currentRoomNum: Int = 0
  private var combat = false

  import Player._

  def receive = {
    case UpdateMessage(message) => {
      is.println(message)
    }
    case InvalidCommand(error) => {
      is.println(error)
    }
    case CheckInput => {
      if (this.os.ready) {
        val input = os.readLine
        if (input.length > 0) {
          if (!combat) playTime(input) else combatMode(input)
        }
      }
    }
    case GotDescription(num, roomName, desc, items, dests, players, ncps) => {
      currentRoomNum = num
      currentRoomName = roomName
      var newDescription = "\nYou're in the " + roomName + ". " + desc + "\nYou can go: \n"
      for (i <- 0 until dests.length) {
        newDescription += dests(i).dir + " to the " + dests(i).name + "\n"
      }
      newDescription += "This room contains: \n"
      for (i <- 0 until items.length) {
        newDescription += items(i).name + "\n"
      }
      newDescription += "Players: \n"
      for (i <- players) {
        newDescription += i + "\n"
      }
      newDescription += "Bugs: \n"
      for (i <- ncps) {
        newDescription += i + "\n"
      }
      is.println(newDescription)
    }
    case GotPath(p) => {
      if (p.length < 2) is.println("You're already here!")
      else {
        is.println("Path to the " + p(p.length - 1) + ": ")
        for (i <- 1 until p.length) is.println(p(i))
      }
    }
    case RoomMessageReceived(from, message) => {
      is.println(from + ": " + message)
    }
    case InvalidRoom => {
      is.println("Not a valid direction. Enter 'look' to see where you can go.")
    }
    case ItemNotFound => {
      is.println("Not a valid item. Enter 'l' to see the available items.")
    }
    case MovedRooms(room) => {
      currentRoom = room
      room ! Room.GetDescription
      currentRoom ! UpdateMessage(this.name + " entered the room.")
    }
    case FirstMove(room, manager, pManager, aManager) => {
      roomManager = manager
      currentRoom = room
      playerManager = pManager
      activityManager = aManager
      firstMove
    }
    case GotItem(item) => {
      inventory ::= item
      currentRoom ! UpdateMessage(this.name + " picked up the " + item.name + ".")
    }
    case DroppedItem(item) => {
      inventory = inventory.filter(i => i.name != item.name)
      currentRoom ! UpdateMessage(this.name + " dropped the " + item.name + ".")
    }
    case AttackBug(bugName) => {
      kill(bugName)
    }
    case ActivityManager.Activity(time, target, targetName, killer, killerName, damage) => {
      health = health - damage
      is.println(killerName + " attacked. Your health: " + this.health)
      if(health < 0) playerDead(killerName)
    }
    case NewMessage(message) => {
      is.println(message)
    }
    case ExitCombatMode => {
      currentTarget = null
      combat = false
      fleeAttempt = true
      activityManager ! ActivityManager.ExitNCPCombat(currentTarget)
    }
    case BugStillHere(bugName) => {
      activityManager ! ActivityManager.AttackNCP(currentTarget, equippedItem, this.name)
    }
    case EnterCombat => combat = true
  }

  def playTime(input: String): Unit = {
    input match {
      case "n"   => move("north")
      case "s"   => move("south")
      case "e"   => move("east")
      case "w"   => move("west")
      case "health" => checkHealth
      case "north"   => move("north")
      case "south"   => move("south")
      case "east"   => move("east")
      case "west"   => move("west")
      case "inv" => printInv
      case "look" => {
        currentRoom ! Room.GetDescription
      }
      case "exit" => is.println("Goodbye!"); quit
      case "help" =>
        for (i <- instrucLines) is.println(i) //; playTime
      case _ =>
        if (input.length > 4 && input.substring(0, 3) == "get") {
          if (inventory.length > 1) is.println("You can only carry two items.") else {
            val i = input.substring(4).trim
            currentRoom ! Room.GetItem(i)
          }
        } else if (input.length > 5 && input.substring(0, 4) == "kill") {
          currentRoom ! Room.CheckNCP(input.substring(5))
        } else if (input.length > 8 && input.substring(0, 7) == "unequip") {
          unequipItem
        } else if (input.length > 6 && input.substring(0, 5) == "equip") {
          equipItem(input.substring(6))
        } else if (input.length > 5 && input.substring(0, 4) == "tell") {
          sendMessage(input.substring(5))
        } else if (input.length > 4 && input.substring(0, 3) == "say") {
          tellRoom(input.substring(4))
        } else if (input.length > 5 && input.substring(0, 4) == "drop") {
          dropItem(input.substring(5))
        } else if (input.length > 13 && input.substring(0, 12) == "shortestPath") {
          shortestPath(input.substring(13))
        } else {
          invalidCommand(input)
        }
    }
  }
  var currentTarget: String = ""
  var fleeAttempt = true
  def combatMode(input: String): Unit = {
    input match {
      case "kill" => {
        currentRoom ! Room.CheckNCPAttackMode(currentTarget)
      }
      case "flee" => attemptFlee
      case _      => is.println("You can only 'kill' or 'flee' in combat mode.")
    }
  }
  def playerDead(bug:String):Unit ={
    is.println("A valient effort, but "+bug+" has killed you.")
    for(i<-inventory) dropItem(i.name)
    currentRoom ! UpdateMessage(bug+" has killed "+this.name+". ")
    quit
  }
  def kill(target: String): Unit = {
    if (equippedItem == null) is.println("You need to equip a weapon for that.") else {
      currentTarget = target
      activityManager ! ActivityManager.AttackNCP(currentTarget, equippedItem, this.name)
    }
  }
  def attemptFlee: Unit = {
    if (fleeAttempt) {
      val flee = util.Random.shuffle(List(0, 1, 2, 3)).head
      if (flee>0) {
        activityManager ! ActivityManager.ExitNCPCombat(currentTarget)
        combat = false
        fleeAttempt = true
        is.println("You successfully escaped.")
      } else is.println("You could not escape. Stay and fight."); fleeAttempt = false
    } else is.println("Your flee already failed.")
  }
  def unequipItem: Unit = {
    equippedItem = null
  }
  def equipItem(newItem: String): Unit = {
    if (equippedItem != null) {
      is.println("Must unequip the " + equippedItem.name)
    } else {
      val tryItem = inventory.filter(_.name == newItem)
      if (tryItem.length > 0) {
        equippedItem = inventory.filter(_.name == newItem).head
        is.println(newItem + " equipped.")
      } else {
        is.println("You don't have that item.")
      }
    }
  }
  def quit: Unit = {
    sock.close()
  }
  def sendMessage(text: String) = {
    val to = text.split(" ", 2)(0)
    val message = text.split(" ", 2)(1)
    playerManager ! PlayerManager.PrivateMessage(to, this.name, message)
  }
  def shortestPath(r: String): Unit = {
    roomManager ! RoomManager.ShortestPath(currentRoomNum, r)
  }
  def dropItem(item: String): Unit = {
    var index = 0
    var flag = true
    var newItem: Item = null
    if(equippedItem != null && item == equippedItem.name) unequipItem
    while (index <= inventory.length && flag) {
      if(index == inventory.length) {
        is.println("You don't have the '" + item + ".' Type 'inv' to see your inventory.")
        flag = false
      } else {
        if (inventory(index).name == item) {
          newItem = inventory(index)
          currentRoom ! Room.DropItem(newItem)
          flag = false
        } else {
          index += 1
        }
      }
    }
  }
  def printInv(): Unit = {
    if (this.inventory.length < 1) {
      is.println("You have no weapons.")
    } else {
      is.println("Your current inventory: ")
      for (i <- 0 until this.inventory.length) is.println(this.inventory(i).name)
    }
  }
  def firstMove(): Unit = {
    currentRoom ! Room.EnterRoom(self, name)
  }
  def move(d: String): Unit = {
    currentRoom ! Room.MoveRooms(d, this.name)
  }

  def invalidCommand(s: String): Unit = {
    is.println("'" + s + "' is not a valid command. Type 'help' to see a list of commands")
  }
  def getItem(item: Item): Unit = {
    inventory ::= item
  }
  def tellRoom(message: String): Unit = {
    currentRoom ! Room.MessageRoom(this.name, message)
  }
  def checkHealth:Unit = {
    is.println("Your health: " + health)
  }

}

object Player {
  case object EnterCombat
  case class PlayerDead(playerName:String, playerRef:ActorRef)
  case class BugStillHere(bugName:String)
  case object ExitCombatMode
  case class NewMessage(message: String)
  case class ReceiveAttack(bugName: String, damage: Int)
  case class AttackBug(bugName: String)
  case class UpdateMessage(message: String)
  case class InvalidCommand(errorMessage: String)
  case object CheckInput
  case class RoomMessageReceived(from: String, message: String)
  case class GotPath(p: List[Any])
  case class FirstMove(r: ActorRef, m: ActorRef, pManager: ActorRef, aManager: ActorRef)
  case class GotDescription(roomNum: Int, roomName: String, roomDesc: String, items: List[Item], dests: Array[RoomManager.Destination], players: List[String], ncps: List[String])
  case object InvalidRoom
  case class DroppedItem(item: Item)
  case object ItemNotFound
  case class GotItem(item: Item)
  case object InvalidDirection
  case class MovedRooms(room: ActorRef)
  case class InitialEnterRoom(player: ActorRef, roomKey: String)
}
