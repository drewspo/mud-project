package mud

import scala.reflect.ClassTag

class BinaryHeap[A: ClassTag](higherP: (A, A) => Boolean)  {
  var heap = new Array[A](10)
  private var back = 1

  def enqueue(o: A): Unit = {
    if (back >= heap.length) {
      val tmp = new Array[A](heap.size * 2)
      for (i <- 1 until back) tmp(i) = heap(i)
      heap = tmp
    }
    var bubble = back
    while(bubble > 1 && higherP(o, heap(bubble/2))) {
      heap(bubble) = heap(bubble/2)
      bubble /= 2
    }
    heap(bubble) = o
    back += 1
  }

  def dequeue(): A = {
    val ret = heap(1)
    back -= 1
    val tmp = heap(back)
    var stone = 1
    var flag = true
    while(stone*2 < back && flag) {
      var higherPChild = stone*2
      if(stone*2+1 < back && higherP(heap(stone*2+1), heap(stone*2))) {
        higherPChild += 1
      }
      if(higherP(tmp, heap(higherPChild))) {
        flag = false
      } else {
        heap(stone) = heap(higherPChild)
        stone = higherPChild
      }
    }
    heap(stone) = tmp
    ret
  }
  
  
  def peek: A = heap(1)

  def isEmpty: Boolean = back == 1
}