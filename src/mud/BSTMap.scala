

package mud

import collection.mutable
import scala.collection.mutable.ArrayStack

class BSTMap[K, V](comp: (K, K) => Int) extends mutable.Map[K, V] {
  import BSTMap._
  private var root: Node[K, V] = null

  def +=(kv: (K, V)): BSTMap.this.type = {
    def helper(r: Node[K, V]): Node[K, V] = {
      if (r == null) new Node[K, V](kv._1, kv._2, null, null)
      else {
        val c = comp(kv._1, r.key)
        if (c == 0) r.value = kv._2
        else if (c < 0) r.left = helper(r.left)
        else r.right = helper(r.right)
        r
      }
    }
    root = helper(root)
    this
  }

  def -=(key: K) = {
    def recur(n: Node[K, V]): Node[K, V] = {
      if (n != null) null
      else {
        val c = comp(key, n.key)
        if (c == 0) {
          if (n.left == null) n.right
          else if (n.right == null) n.left
          else {
            val (k, v, node) = removeMax(n.left)
            n.left = node
            n.key = k
            n.value = v
            n
          }
        } else if (c < 0) {
          n.left = recur(n.left)
          n
        } else {
          n.right = recur(n.right)
          n
        }
      }
    }
    def removeMax(n: Node[K, V]): (K, V, Node[K, V]) = {
      if (n.right == null) {
        (n.key, n.value, n.left)
      } else {
        val (k, v, node) = removeMax(n.right)
        n.right = node
        (k, v, n)
      }
    }
    root = recur(root)
    this
  }

  def get(key: K): Option[V] = {
    def helper(r: Node[K, V]): Option[V] = {
      if (r == null) None
      else {
        val c = comp(key, r.key)
        if (c == 0) Some(r.value)
        else if (c < 0) helper(r.left)
        else helper(r.right)
      }
    }
    helper(root)
  }

  def iterator = new Iterator[(K, V)] {
    val stack = new ArrayStack[Node[K, V]]()
    def runLeft(n: Node[K, V]): Unit = {
      var rover = n
      while (rover != null) {
        stack.push(rover)
        rover = rover.left
      }
    }
    runLeft(root)
    def next: (K, V) = {
      val n = stack.pop()
      runLeft(n.right)
      (n.key, n.value)
    }
    def hasNext: Boolean = !stack.isEmpty

  }
}

object BSTMap {
  class Node[K, V](var key: K, var value: V, var left: Node[K, V], var right: Node[K, V])
}