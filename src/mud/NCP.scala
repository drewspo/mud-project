package mud

import scala.util.Random

import akka.actor.Actor
import akka.actor.ActorRef

class NCP(name: String, var health: Int) extends Actor {
  private var alive = true
  val r = scala.util.Random
  var numRooms = 0
  private var activityManager: ActorRef = null
  private var currentRoom: ActorRef = null
  private var roomManager: ActorRef = null
  private var roomMap = new BSTMap[Int, ActorRef]((a: Int, b: Int) => a - b)
  import NCP._
  def receive = {
    case RoomMapReceived(map) => {
      roomMap = map
      numRooms = map.size
      initializeNCP
    }
    case InitializeNCP(manager, aManager) => {
      roomManager = manager
      activityManager = aManager
      getRoomMap
    }
    case MoveRooms(newRoom) => {
      moveRooms(newRoom)
    }
    case ActivityManager.Activity(time, target, targetName, killer, killerName, damage) => {
      health -= damage
      if (health < 0) {
        currentRoom ! Player.UpdateMessage(killerName + " has slain " + targetName + ".")
        killer ! Player.ExitCombatMode
        bugDead
      } else {
        val t = r.nextInt(10)
        activityManager ! ActivityManager.AttackPlayer(killerName, t, name, r.nextInt(15))
        killer ! Player.NewMessage("Direct hit! " + this.name + " Health: " + this.health)
      }
    }
  }
  def initializeNCP: Unit = {
    val walk = new Thread(new Runnable {
      def run() {
        while (alive) {
          if (currentRoom != null) leaveRoom(currentRoom)
          val newRoom = roomMap(r.nextInt(numRooms))
          if (currentRoom != newRoom) {
            currentRoom = newRoom
            moveRooms(currentRoom)
          }
          val sleep = scala.util.Random
          val s = sleep.nextInt(20000)
          Thread.sleep(s + 10000)
        }
      }
    })
    walk.start
  }
  def bugDead: Unit = {
    alive = false
    activityManager ! ActivityManager.BugDied(this.name, self)
    currentRoom ! ActivityManager.BugDied(this.name, self)
  }
  def leaveRoom(curRoom: ActorRef): Unit = {
    currentRoom ! Room.NCPLeave(this.name)
  }
  def getRoomMap: Unit = {
    roomManager ! RoomManager.NCPRoom
  }
  def moveRooms(newRoom: ActorRef): Unit = {
    currentRoom ! Room.NCPEnter(this.name)
  }
}
object NCP {
  case class ReceiveDamage(damage: Int, killerName: String)
  case class RoomMapReceived(map: BSTMap[Int, ActorRef])
  case class InitializeNCP(manager: ActorRef, aManager: ActorRef)
  case class MoveRooms(newRoom: ActorRef)
}
