package mud

import akka.actor.Actor
import akka.actor.ActorRef
import scala.xml.Node

class Room(val num: Int, val keyword: String, val name: String, val desc: String, private var items: List[Item], exitKeys: Array[RoomManager.Destination]) extends Actor {
  private var characters: List[ActorRef] = Nil
  private var charNames = List[String]()
  private var exits: BSTMap[Int, ActorRef] = null
  private var roomNames: List[RoomManager.RoomName] = null
  private var NCPs = List[ActorRef]()
  private var NCPNames = List[String]()

  import Room._

  def receive = {
    case RemovePlayer(playerName, playerRef) => {
      removePlayer(playerRef, playerName)
    }
    case Player.UpdateMessage(message) => {
      for (c <- characters) c ! Player.UpdateMessage(message)
    }
    case MessageRoom(from, message) => {
      for (p <- characters) p ! Player.RoomMessageReceived(from, message)
    }
    case RoomName => {
      sender ! name
    }
    case GetDescription => {
      sender ! Player.GotDescription(this.num, this.name, desc, items, exitKeys, charNames, NCPNames)
    }
    case LinkExits(rooms, names) =>
      exits = rooms
      roomNames = names
    case GetItem(item) =>
      var index = 0
      var flag = true
      var newItem: Item = null
      while (flag && index <= items.length) {
        if (index == items.length) {
          sender ! Player.ItemNotFound
          flag = false
        } else if (item == items(index).name) {
          newItem = items(index)
          items = items.filter(a => a.name != items(index).name)
          sender ! Player.GotItem(newItem)
          flag = false
        } else {
          index += 1
        }
      }
    case DropItem(item) => {
      items ::= item
      sender ! Player.DroppedItem(item)
    }
    case MoveRooms(dest: String, playerName:String) => {
      val i = getIndex(dest)
      if (i < 0) {
        sender ! Player.InvalidRoom
      } else {
        val newRoom = exits(i)
        newRoom ! Room.EnterRoom(sender, playerName)
        removePlayer(sender, playerName)
        for (c <- characters) c ! Player.UpdateMessage(playerName+" has left the room.")
      }
    }
    case EnterRoom(player, n) => {
      addPlayer(player, n)
      player ! Player.MovedRooms(self)
    }
    case NCPEnter(ncpName) => {
      NCPs ::= sender
      NCPNames ::= ncpName
      for (c <- characters) c ! Player.UpdateMessage(ncpName + " has entered the room.")
    }
    case NCPLeave(ncpName) => {
      for (c <- characters) c ! Player.UpdateMessage(ncpName + " has left the room.")
      NCPs = NCPs.filter(p => p != sender)
      NCPNames = NCPNames.filter(p => p != ncpName)
    }
    case CheckNCP(bugName) => {
      val newList = NCPNames.filter(_ == bugName)
      if (newList.length > 0) {
        sender ! Player.AttackBug(bugName)
      } else {
        sender ! Player.InvalidCommand(bugName + " isn't here.")
        sender ! Player.ExitCombatMode
      }
    }
    case ActivityManager.BugDied(bugName, bugRef) => {
      bugDied(bugName, bugRef)
    }
    case CheckNCPAttackMode(bugName) => {
      val newList = NCPNames.filter(_ == bugName)
      if (newList.length > 0) {
        sender ! Player.BugStillHere(bugName)
      } else {
        sender ! Player.InvalidCommand(bugName + " isn't here.")
        sender ! Player.ExitCombatMode
      }
    }
    case Player.PlayerDead(playerName, playerRef) => {
      playerDied(playerName, playerRef)
    }
  }
  def playerDied(playerName: String, playerRef: ActorRef) = {
    characters = characters.filter(_ != playerRef)
    charNames = charNames.filter(_ != playerName)
  }
  def bugDied(bugName: String, bugRef: ActorRef) = {
    NCPs = NCPs.filter(_ != bugRef)
    NCPNames = NCPNames.filter(_ != bugName)
  }
  def getIndex(keyWord: String): Int = {
    var index = 0
    var flag = true
    while (index < this.exitKeys.length && flag) {
      if (this.exitKeys(index).dir == keyWord) {
        flag = false
      } else {
        index += 1
      }
    }
    if (index == this.exitKeys.length) -1 else this.exitKeys(index).num
  }
  def addPlayer(player: ActorRef, n: String) = {
    charNames ::= n
    characters ::= player
  }
  def removePlayer(player: ActorRef, pName: String) = {
    characters = characters.filter(p => p != player)
    charNames = charNames.filter(p => p != pName)
  }
}

object Room {
  case class CheckNCPAttackMode(bugName: String)
  case class CheckNCP(bugName: String)
  case class NCPEnter(ncpName: String)
  case class NCPLeave(ncpName: String)
  case class RemovePlayer(playerName: String, playerRef: ActorRef)
  case class MessageRoom(from: String, message: String)
  case object RoomName
  case class MoveRooms(d: String, n: String)
  case class LinkExits(rooms: BSTMap[Int, ActorRef], roomNames: List[RoomManager.RoomName])
  case class TakeExit(d: Int)
  case class EnterRoom(player: ActorRef, n: String)
  case object GetDescription
  case class InitialEnterRoom(player: ActorRef, roomKey: String)
  case class DropItem(item: Item)
  case class GetItem(item: String)
  case class MovedRooms(room: ActorRef)
  case object InvalidDirection
}           