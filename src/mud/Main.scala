package mud

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintStream
import java.net.ServerSocket
import java.net.Socket
import scala.collection.mutable
import scala.io.Source
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import scala.concurrent.Future

object Main {
  case class User(sock: Socket, is: BufferedReader, ps: PrintStream, name: String)
  val users = new mutable.ArrayBuffer[ActorRef]() //with mutable.SynchronizedBuffer[Player] {}
  val playerNames = new mutable.ArrayBuffer[String]()
  val intro = Source.fromFile("intro.txt")
  val introLines = intro.getLines.toArray
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("MudSystem")
    val ss = new ServerSocket(4444)
    val roomManager = system.actorOf(Props(new RoomManager))
    val playerManager = system.actorOf(Props(new PlayerManager))
    val activityManager = system.actorOf(Props(new ActivityManager))
    val queueUpdates = new Thread (new Runnable {
      def run() {
        while(true) {
          activityManager ! ActivityManager.CheckQueue
          Thread.sleep(100)
        }
      }
    })
    queueUpdates.start
    val updates = new Thread(new Runnable {
      def run() {
        while (true) {
          playerManager ! PlayerManager.RunUpdates
          Thread.sleep(100)
        }
      }
    })
    updates.start
    def initializeNCPs: Unit = {
      activityManager ! ActivityManager.InitializeNCPs(roomManager)
    }
    val accept = new Thread(new Runnable {
      def run() {
        while (true) {
          val sock = ss.accept()
          val is = new BufferedReader(new InputStreamReader(sock.getInputStream()))
          val os = new PrintStream(sock.getOutputStream())
          for (i <- introLines) os.println(i)
          val getName = new Thread(new Runnable {
            def run() {
              os.println("What is your name?")
              var playerName = is.readLine()
              nameTaken(playerName)
              def nameTaken(pName: String): Unit = {
                if (!playerNames.forall(_ != pName)) {
                  os.println("Sorry, " + "'" + pName + "'" + " is already taken. Enter a new name: ")
                  nameTaken(is.readLine())
                } else if (pName.split(" ").length>1) {
                  os.println("Only first names allowed! Enter a new name: ")
                  nameTaken(is.readLine())
                } else {
                  playerName = pName
                }
              }
              os.println("Hello, " + playerName + "! " + "Let's kill some bugs.")
              val newPlayer = system.actorOf(Props(new Player(playerName, List[Item](), sock, os, is, 100)))
              users += newPlayer
              if (users.length == 1) initializeNCPs
              playerNames += playerName
              playerManager ! PlayerManager.NewPlayer(playerName, newPlayer)
              activityManager ! PlayerManager.NewPlayer(playerName, newPlayer)
              roomManager ! RoomManager.InitialEnterRoom(newPlayer, 0, users.length, playerManager, activityManager)
            }
          })
          getName.run
          Thread.sleep(100)
        }
      }
    })
    accept.run
  }
}
