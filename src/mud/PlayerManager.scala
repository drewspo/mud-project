

package mud

import akka.actor.Actor
import akka.actor.ActorRef
import scala.collection.mutable

class PlayerManager extends Actor {

  import PlayerManager._

  val players = mutable.ArrayBuffer[NewPlayer]()
  def receive = {
    case PrivateMessage(to, from, message) => {
      if(players.filter(_.playerName==to).length==0) {
        sender ! Player.InvalidCommand("Player not found.")
      } else {
      val recipient = players.filter(_.playerName==to)(0).playerRef
      recipient ! Player.RoomMessageReceived(from, message)
      }
    }
    case NewPlayer(name, player) => {
      players += NewPlayer(name, player)
    }
    case RunUpdates => {
      for (p <- players) {
        p.playerRef ! Player.CheckInput
      }
    }
    case Room.RemovePlayer(pName, playerRef) => {
      players -= NewPlayer(pName, playerRef)
    }
    case Player.PlayerDead(playerName, playerRef) => {
      players -= NewPlayer(playerName, playerRef)
    }
  }
}

object PlayerManager {
  case object RunUpdates
  case class NewPlayer(playerName: String, playerRef: ActorRef)
  case class PrivateMessage(to:String, from:String, message:String)

}