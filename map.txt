6
0 hallway
This hallway is swarming with bees - make it past without getting stung.   
2
north 2 den
east 1 bedroom
1
vase 5
1 bedroom
Your mattress has become infested with bed bugs... as you walk in your room, the miniscule critters glance up from their tempur-pedic snack. They aren't happy.   
1
west 0 hallway
2
vacuum cleaner 10
notebook 3
2 den
The "family room" has taken on a whole new meaning... armies of fire ants march around the room, setting up shop for generations to come. 
3
south 0 hallway
east 3 garage
west 5 kitchen
2
pillow 6
boomerang 8
3 garage
Forgetting to take out last week's garbage has come back to haunt you. A gang of flies dances zealously around the trash can.   
1
west 2 den
3
repellent 7
chainsaw 9
plastic bag 10
4 patio
You step outside but suddenly feel stuck...you look down to find a city of spiders, all spinning their webs about the patio. 
1
south 2 den
2
pesticide 3
laser 2
5 kitchen
Humans aren't the only ones that like oreos. A fierce colony of ants has descended upon your pantry, taking whatever they can find.   
2
north 4 patio
east 2 den
1
water gun 4



